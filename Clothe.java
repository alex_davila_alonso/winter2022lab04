public class Clothe{
	private String size;
	private String madeFrom;
	private int stock;
	
	public void countStock(){
		if(this.stock > 10){
			System.out.println("There is a lot of clothes to count!");
		}
		else{
			System.out.println("There is not a lot of clothes to count!");
		}
	}
	//getter methods
	public String getSize(){
		return this.size;
	}
	public String getMadeFrom(){
		return this.madeFrom;
	}
	public int getStock(){
		return this.stock;
	}
	
	//setter methods
	public void setSize(String newSize){
		this.size = newSize;
	}
	public void setMadeFrom(String newMadeFrom){
		this.madeFrom = newMadeFrom;
	}
	public void setStock(int newStock){
		if(newStock >= 0){
			this.stock = newStock;
		} 
	}
	
	//constructor
	public Clothe(String size, String madeFrom, int stock){
		this.size = size;
		this.madeFrom = madeFrom;
		this.stock = stock;
	} 
}