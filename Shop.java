import java.util.Scanner;
public class Shop{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		
		Clothe[] shirt = new Clothe[4];
		for(int i=0 ; i < shirt.length ; i++){
			System.out.println("Please input in order the following info: Size, What the shirt is made from and the stock!");
			shirt[i]=new Clothe(input.nextLine(),input.nextLine(),Integer.parseInt(input.nextLine()));
		}
		//updating the fields for the last products using setter methods
		System.out.println("Please enter different info on the last shirt!");
		
		//changing value of size for last item
		System.out.println("Size: ");
		System.out.println("Before Call to Setter:");
		System.out.println(shirt[3].getSize());
		shirt[3].setSize(input.nextLine());
		System.out.println("After Call to Setter:");
		System.out.println(shirt[3].getSize());
		
		//Changing value of madeFrom of last item
		System.out.println("What its made from: ");
		System.out.println("Before Call to Setter:");
		System.out.println(shirt[3].getMadeFrom());
		shirt[3].setMadeFrom(input.nextLine());
		System.out.println("After Call to Setter:");
		System.out.println(shirt[3].getMadeFrom());
		
		//changing value of Stock for last item
		System.out.println("Stock: ");
		System.out.println("Before Call to Setter:");
		System.out.println(shirt[3].getStock());
		shirt[3].setStock(Integer.parseInt(input.nextLine()));
		System.out.println("After Call to Setter:");
		System.out.println(shirt[3].getStock());
		
		//previous lab 
		System.out.println("The last shirt size is: "+shirt[3].getSize()+" it is made from: "+shirt[3].getMadeFrom()+" and we have this many in stock: "+shirt[3].getStock());
		
		shirt[3].countStock();
		
	}
}